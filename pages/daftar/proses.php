<?php
// Panggil koneksi database.php untuk koneksi database
require_once "../../config/database.php";

if (isset($_POST['daftar'])) {
	// ambil data hasil submit dari form
	$nama       = mysql_real_escape_string(trim($_POST['nama']));
	$email      = mysql_real_escape_string(trim($_POST['email']));
	$password   = md5(mysql_real_escape_string(trim($_POST['password'])));
	$alamat     = mysql_real_escape_string(trim($_POST['alamat']));
	$id_prov    = mysql_real_escape_string(trim($_POST['id_prov']));
	$kabkota    = mysql_real_escape_string(trim($_POST['kabkota']));
	$kode_pos   = mysql_real_escape_string(trim($_POST['kode_pos']));
	$no_telepon = mysql_real_escape_string(trim($_POST['no_telepon']));

	// perintah query untuk pengecekan email pada tabel konsumen
	$query_email = mysql_query("SELECT email FROM tbl_konsumen WHERE email='$email'")
										  or die('Ada kesalahan pada query cek email : '.mysql_error());
	$row_email   = mysql_num_rows($query_email);

	// jika data email sudah ada
	if ($row_email > 0) {
		// maka alihkan ke halaman form pendaftaran
		header("location: ../../main.php?page=daftar&alert=1");
	}
	// jika data email belum ada
	else {
		// maka jalankan perintah query untuk menyimpan data ke tabel pesan
		$query = mysql_query("INSERT INTO tbl_konsumen
													  VALUES('',NOW(),'$nama',
													  		 '$alamat',
															 '$kabkota',
															 '$id_prov',
															 '$kode_pos',
															 '$no_telepon',
															 '$email',
															 '$password',
                               0)")
									or die('Ada kesalahan pada query insert : '.mysql_error());
		// cek query
		if ($query) {
      // Kode untuk generate token berdasarkan angka hari, bulan, tahun, jam sampai milidetik agar unik
      $t = microtime(true);
      $micro = sprintf("%06d",($t - floor($t)) * 1000000);
      $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
      $token = $d->format("YmdHisu");
      
      // Simpan token untuk konfirmasi akun
      $query_token = mysql_query("INSERT INTO tbl_token VALUES ('".$token."','".$email."', 0);");
      if($query_token){
        // Kode untuk kirim email menggunakan smtp server
        $from = "noreply@ponselshope.com";
        $to = $email;
        $subject = "Konfirmasi Akun Anda Di PonselShope";
        $message = "Terima kasih telah melakukan pembuatan akun di PONSELSHOP. Silahkan akses http://ponselshope.mandan.online/main.php?page=konfirmasi-akun&token=$token untuk melakukan aktivasi akun dan mulai berbelanja";
        $headers = "From:" . $from;
        mail($to,$subject,$message, $headers);
        
        // jika berhasil tampilkan pesan berhasil simpan data
        header("location: ../../main.php?page=daftar&alert=2");
      }else{
        echo "Terdapat kesalahan pada sistem. Silahkan coba beberapa saat lagi.";
      }
		}
	}
}
?>
