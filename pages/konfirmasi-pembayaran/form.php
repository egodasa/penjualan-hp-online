<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman login dan tampilkan pesan = 1
if (empty($_SESSION['user_email']) && empty($_SESSION['user_password'])){
    echo "<script type='text/javascript'>alert('Anda harus login terlebih dahulu!');</script>
          <meta http-equiv='refresh' content='0; url=?page=home'>";
}
// jika user sudah login, maka jalankan perintah untuk ubah password
else {
    if ($_GET['form']=='add') { ?>
       <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i style="margin-right:6px" class="fa fa-retweet"></i>
                            Konfirmasi Pembayaran
                        </h3>
                       
                    </div>
                </div>
<?php

$sql_edit = mysql_query("SELECT * FROM tbl_transaksi WHERE id_transaksi='$_GET[transaksi]'");


	    $row =  mysql_fetch_array($sql_edit);

?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                  <!-- tampilan form hubungi kami -->
                                <form class="form-horizontal" method="POST" action="pages/konfirmasi-pembayaran/proses.php" enctype="multipart/form-data">

                                 
                                    <input type="hidden" name="id_transaksi" value="<?php echo $_GET['transaksi']; ?>">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Tanggal Pembayaran</label>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control date-picker" data-date-format="dd-mm-yyyy" name="tanggal_bayar" autocomplete="off" required>
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Rekening Asal</label>
                                        <div class="col-sm-4">
                                            <select style="font-color:#555" class="form-control" name="rekening_asal" required>
                                                <option value=""></option>
                                                <option value="BI">Bank Indonesia</option>                                     
                                                <option value="Mandiri">Mandiri</option>
												<option value="BNI">BNI</option>
												<option value="BRI">BRI</option>
												<option value="BTN">BTN</option>
												<option value="BRI Agroniaga">BRI Agroniaga</option>
												<option value="Anda">Anda</option>
												<option value="Artha Graha Internasional">Artha Graha Internasional</option>
												<option value="Bukopin">Bukopin</option>
											  	<option value="Bumi Arta">Bumi Arta</option>
												<option value="Bank Capital Indonesia">Bank Capital Indonesia</option>
                                                <option value="BCA">BCA</option>
                                                <option value="CIMB Niaga">CIMB Niaga</option>
                                                <option value="Danamon">Danamon</option>
                                                <option value="Ekonomi Raharja">Ekonomi Raharja</option>
                                                <option value="Ganesha">Ganesha</option>
												<option value="Hana">Hana</option>                                              
                                                <option value="Woori Saudara">Woori Saudara</option>
                                                <option value="ICBC">ICBC</option>
                                                <option value="Index Selindo">Index Selindo</option>
                                                <option value="Maybank">Maybank</option>
                                                <option value="Maspion">Maspion</option>
                                                <option value="Mayapada">Mayapada</option>
												<option value="Mega">Mega</option>
                                                <option value="Mestika Dharma">Mestika Dharma</option>
                                                <option value="Shinhan">Shinhan</option>
                                                <option value="MNC">MNC</option>
                                                <option value="J Trust">J Trust</option>
                                                <option value="Nusantara Parahyangan">Nusantara Parahyangan</option>
                                                <option value="OCBC NISP">OCBC NISP</option>
                                                <option value="Bank of India Indonesia">Bank of India Indonesia</option>
												<option value="Panin Bank">Panin Bank</option>
                                                <option value="Bank Permata">Bank Permata</option>
                                                <option value="QNB">QNB</option>
                                                <option value="SBI">SBI</option>
                                                <option value="Sinarmas">Sinarmas</option>
                                                <option value="UOB">UOB</option>
                                                <option value="Amar Bank Indonesia">Amar Bank Indonesia</option>
                                                <option value="Bank Andara">Bank Andara</option>
												 <option value="Bank Artos Indonesia">Bank Artos Indonesia</option>
                                                <option value="Bank Bisnis Internasional">Bank Bisnis Internasional</option>
                                                <option value="Bank Tabungan Pensiunan Nasional">Bank Tabungan Pensiunan Nasional</option>
                                                <option value="Bank Sahabat Sampoerna">Bank Sahabat Sampoerna</option>
                                                <option value="Bank Fama Internasional">Bank Fama Internasional</option>
												<option value="Bank Harda Internasional">Bank Harda Internasional</option>
                                                <option value="Bank Ina Perdana">Bank Ina Perdana</option>
                                                <option value="Bank Jasa Jakarta">Bank Jasa Jakarta</option>
                                                <option value="Bank Kesejahteraan Ekonomi">Bank Kesejahteraan Ekonomi</option>
                                                <option value="Bank Dinar Indonesia">Bank Dinar Indonesia</option>
                                                <option value="Bank Mayora">Bank Mayora</option>
                                                <option value="Bank Mitraniaga">Bank Mitraniaga</option>
                                                <option value="Bank Multi Arta Sentosa">Bank Multi Arta Sentosa</option>
												<option value="Bank Nationalnobu">Bank Nationalnobu</option>
                                                <option value="Prima Master Bank">Prima Master Bank</option>
                                                <option value="Bank Pundi Indonesia">Bank Pundi Indonesia</option>
                                                <option value="Bank Royal Indonesia">Bank Royal Indonesia</option>
                                                <option value="Bank Mandiri Taspen Pos">Bank Mandiri Taspen Pos</option>
                                                <option value="Bank Victoria Internasional">Bank Victoria Internasional</option>
                                                <option value="Bank Yudha Bhakti">Bank Yudha Bhakti</option>
                                                <option value="Bank BPD Aceh">Bank BPD Aceh</option>
												<option value="Bank Sumut">Bank Sumut</option>
                                                <option value="Bank Nagari">Bank Nagari</option>
                                                <option value="Bank Riau Kepri">Bank Riau Kepri</option>
                                                <option value="Bank Jambi">Bank Jambi</option>
                                                <option value="Bank Bengkulu">Bank Bengkulu</option>
                                                <option value="Bank Sumsel Babel">Bank Sumsel Babel</option>
                                                <option value="Bank Lampung">Bank Lampung</option>
                                                <option value="Bank DKI">Bank DKI</option>
												<option value="Bank BJB">Bank BJB</option>
                                                <option value="Bank Jateng">Bank Jateng</option>
                                                <option value="Bank BPD DIY">Bank BPD DIY</option>
                                                <option value"Bank Jatim">Bank Jatim</option>
                                                <option value="Bank Kalbar">Bank Kalbar</option>
												<option value="Bank Kalteng">Bank Kalteng</option>
                                                <option value="Bank Kalsel">Bank Kalsel</option>
                                                <option value="Bank Kaltim">Bank Kaltim</option>
                                                <option value="Bank Sulsel">Bank Sulsel</option>
                                                <option value="Bank Sultra">Bank Sultra</option>
                                                <option value="Bank BPD Sulteng">Bank BPD Sulteng</option>
                                                <option value="Bank Sulut">Bank Sulut</option>
                                                <option value="Bank BPD Bali">Bank BPD Bali</option>
												<option value="Bank NTB">Bank NTB</option>
                                                <option value="Bank NTT">Bank NTT</option>
                                                <option value="Bank Maluku">Bank Maluku</option>
                                                <option value="Bank Papua">Bank Papua</option>
                                                <option value="Bank ANZ Indonesia">Bank ANZ Indonesia</option>
                                                <option value="Bank Commonwealth">Bank Commonwealth</option>
                                                <option value="Bank Agris">Bank Agris</option>
                                                <option value="Bank BNP Paribas Indonesia">Bank BNP Paribas Indonesia</option>
												<option value="Bank Capital Indonesia">Bank Capital Indonesia</option>
                                                <option value="Bank Chinatrust Indonesia">Bank Chinatrust Indonesia</option>
                                                <option value="Bank DBS Indonesia">Bank DBS Indonesia</option>
												<option value="Bank Mizuho Indonesia">Bank Mizuho Indonesia</option>
                                                <option value="Bank Rabobank International Indonesia">Bank Rabobank International Indonesia</option>
                                                <option value="Bank Resona Perdania">Bank Resona Perdania</option>
                                                <option value="Bank Sumitomo Mitsui Indonesia">Bank Sumitomo Mitsui Indonesia</option>
                                                <option value="Bank Windu Kentjana International">Bank Windu Kentjana International</option>
												<option value="Bank of America">Bank of America</option>
                                                <option value="Bangkok Bank">Bangkok Bank</option>
                                                <option value="Bank of China">Bank of China</option>
                                                <option value="Citibank">Citibank</option>
                                                <option value="Deutsche Bank">Deutsche Bank</option>
                                                <option value="HSBC">HSBC</option>
                                                <option value="JPMorgan Chase">JPMorgan Chase</option>
                                                <option value="Standard Chartered">Standard Chartered</option>
												<option value="The Bank of Tokyo-Mitsubishi UFJ">The Bank of Tokyo-Mitsubishi UFJ</option>
                                                <option value="Bank BNI Syariah">Bank BNI Syariah</option>
                                                <option value="Bank Mega Syariah">Bank Mega Syariah</option>
                                                <option value="Bank Muamalat Indonesia">Bank Muamalat Indonesia</option>
                                                <option value="Bank Syariah Mandiri">Bank Syariah Mandiri</option>
                                                <option value="BCA Syariah">BCA Syariah</option>
                                                <option value="Bank BJB Syariah">Bank BJB Syariah</option>
                                                <option value="Bank BRI Syariah">Bank BRI Syariah</option>
												<option value="Panin Bank Syariah">Panin Bank Syariah</option>
												<option value="Bank Syariah Bukopin">Bank Syariah Bukopin</option>
                                                <option value="Bank Victoria Syariah">Bank Victoria Syariah</option>
                                                <option value="BTPN Syariah">BTPN Syariah</option>
                                                <option value="Bank Maybank Syariah Indonesia">Bank Maybank Syariah Indonesia</option>
                                                <option value="Bank BTN Syariah">Bank BTN Syariah</option>
												<option value="Bank Danamon Syariah">Bank Danamon Syariah</option>
                                                <option value="CIMB Niaga Syariah">CIMB Niaga Syariah</option>
                                                <option value="BII Syariah">BII Syariah</option>
                                                <option value="OCBC NISP Syariah">OCBC NISP Syariah</option>
                                                <option value="Bank Permata Syariah">Bank Permata Syariah</option>
                                                <option value="Bank Nagari Syariah">Bank Nagari Syariah</option>
                                                <option value="Bank BPD Aceh Syariah">Bank BPD Aceh Syariah</option>
                                                <option value="Bank DKI Syariah">Bank DKI Syariah</option>
												<option value="Bank Kalbar Syariah">Bank Kalbar Syariah</option>
                                                <option value="Bank Kalsel Syariah">Bank Kalsel Syariah</option>
                                                <option value="Bank NTB Syariah">Bank NTB Syariah</option>
                                                <option value="Bank Riau Kepri Syariah">Bank Riau Kepri Syariah</option>
                                                <option value="Bank Sumsel Babel Syariah">Bank Sumsel Babel Syariah</option>
                                                <option value="Bank Sumut Syariah">Bank Sumut Syariah</option>
                                                <option value="Bank Kaltim Syariah">Bank Kaltim Syariah</option>
                                      
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">No. Rekening Asal</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="no_rekening_asal" autocomplete="off" onKeyPress="return goodchars(event,'0123456789',this)" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Nama Pemilik Rekening Asal</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="pemilik_rekening" autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Rekening Tujuan</label>
                                        <div class="col-sm-4">
                                            <select style="font-color:#555" class="form-control" name="rekening_tujuan" required>
                                                <option value=""></option>
                                                <option value="BNI">BNI</option>
                                                <option value="BCA">BCA</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Jumlah Pembayaran</label>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" class="form-control" id="jumlah_pembayaran" name="jumlah_pembayaran" autocomplete="off" onKeyPress="return goodchars(event,'0123456789',this)"  value="<?php echo $row['total_bayar'];?>"required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Bukti Pembayaran</label>
                                        <div class="col-sm-4">
                                            <input style="margin-top:2px;height:35px" type="file" name="gambar" required> * Max Ukuran Gambar 1 MB
                                        </div>
                                    </div>

                                    <hr/>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <input style="width:100px" type="submit" class="btn btn-primary btn-submit" name="simpan" value="Simpan">
                                        </div>
                                    </div>
									
								 <div class="panel panel-default">
                        <div class="panel-body">
                            <h4>Rekening Pembayaran</h4>
                            <br>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <strong>BCA</strong><br>
                                            Nomor rekening: <strong>547401045678</strong> <br>
                                            Atas Nama: <strong>PONSEL SHOP</strong>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <strong>BNI</strong> <br>
                                            Nomor rekening: <strong>576598786444</strong> <br>
                                            Atas Nama: <strong>PONSEL SHOP</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                        </div>
                    </div> 
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    <?php
    }
    ?>
<?php
}
?>
