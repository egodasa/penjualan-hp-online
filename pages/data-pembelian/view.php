<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman login dan tampilkan pesan = 1
if (empty($_SESSION['user_email']) && empty($_SESSION['user_password'])){
    echo "<script type='text/javascript'>alert('Anda harus login terlebih dahulu!');</script>
          <meta http-equiv='refresh' content='0; url=?page=home'>";
}
// jika user sudah login, maka jalankan perintah untuk ubah password
else {
    if ($_GET['form']=='view') { ?>
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i style="margin-right:6px" class="fa fa-shopping-cart"></i>
                            Data Pembelian
                        </h3>
                  
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr >
                                                <th>No.</th>
												
                                                <th>No Transaksi</th>
                                                <th>Tanggal Transaksi</th>
                                                <th>Jumlah</th>
                                                <th>Total Pembayaran</th>
                                                <th>Status</th>
                                                <th>No Resi</th>
												<th>Layanan Pengiriman</th>
												<th>Tanggal Kirim</th>
                                                <th></th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        <?php
                                        $no = 1;
                                        $query = mysql_query("SELECT * FROM tbl_transaksi
                                                                        WHERE id_konsumen='$_SESSION[id_konsumen]'
                                                                        ORDER BY id_transaksi DESC")
                                                                        or die('Ada kesalahan pada query transaksi: '.mysql_error());

                                        while ($data = mysql_fetch_assoc($query)) {
                                            $tgl               = substr($data['tanggal_transaksi'],0,10);
                                            $exp               = explode('-',$tgl);
                                            $tanggal_transaksi = tgl_eng_to_ind($exp[2]."-".$exp[1]."-".$exp[0]);

 $tgl1               = substr($data['tgl_kirim'],0,10);
                                            $exp               = explode('-',$tgl1);
                                            $tgl_kirim = tgl_eng_to_ind($exp[2]."-".$exp[1]."-".$exp[0]);
                                            $query1 = mysql_query("SELECT SUM(jumlah_beli) as jumlah FROM tbl_transaksi_detail
                                                                        WHERE id_transaksi='$data[id_transaksi]'")
                                                                        or die('Ada kesalahan pada query detail: '.mysql_error());

                                            $data1 = mysql_fetch_assoc($query1);
											
											$tgljthtmp		= date("d-m-Y",strtotime("+3 days", strtotime($data["tanggal_transaksi"])));
											$tglskrng		= date("d-m-Y");
											$idt	= $data["id_transaksi"];
											
											if (($data['statuspengiriman']=="Menunggu Pembayaran") && ($tgljthtmp < $tglskrng)) {
												$batalpesanan	= mysql_query("update tbl_transaksi set statuspengiriman = 'Pesanan Dibatalkan'
													where id_transaksi = '$idt'") or die (mysql_error());
													}
											
                                        ?>
                                            <tr>
                                                <td width='30' class='center'><?php echo $no; ?></td>
												
                                                <td width='80'><?php echo $data['id_transaksi']; ?></td>
                                                <td width='100'><?php echo $tanggal_transaksi; ?></td>
                                                <td width='80'><?php echo $data1['jumlah']; ?> Barang</td>
                                                <td width='100' >Rp. <?php echo format_rupiah_nol($data['total_bayar']); ?></td>
                                                <td width='120'><?php echo $data['statuspengiriman']; ?></td>
                                                <td width='120'><?php echo $data['no_resi']; ?></td>
												<td width='80'><?php echo $data['kurir']; ?></td>
												<td width='100'><?php echo $tgl_kirim; ?></td>
                                                <td width='50' class="center">
                                                    <div>
                                                        <a data-toggle="tooltip" data-placement="top" title="Detail" class="btn btn-primary btn-sm" href="?page=pembelian&form=detail&transaksi=<?php echo $data['id_transaksi']; ?>">
                                                            Detail
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php
                                            $no++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- /.panel -->
                    </div> <!-- /.col -->
                </div> <!-- /.row -->
            </div>
        </div>
        <!-- /.row -->
    <?php
    }

    if ($_GET['form']=='detail') {
        $query = mysql_query("SELECT * FROM tbl_transaksi 
                                        WHERE id_transaksi='$_GET[transaksi]'")
                                        or die('Ada kesalahan pada query tampil data konsumen: '.mysql_error());

        $data = mysql_fetch_assoc($query);

        $nmpenerima   = $data['nama_penerima'];
        $alamat = $data['alamat'];
        $nama_kabkota = $data['id_kota'];
        $nama_provinsi = $data['id_prov'];
        $kode_pos = $data['kodepos'];
        $telepon = $data['nohp'];
    ?>
       <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i style="margin-right:6px" class="fa fa-shopping-cart"></i>
                            Detail Pembelian
                        </h3>
                     
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4>Alamat Tujuan</h3>
                                <p>
                                    <i style="margin-right:7px" class="fa fa-user"></i>
                                    <?php echo $nmpenerima; ?>
                                </p>
                                <p>
                                    <i style="margin-right:7px" class="fa fa-map-marker"></i>
                                    <?php echo $alamat; ?>, <?php echo $nama_kabkota; ?>, <?php echo $nama_provinsi; ?>, <?php echo $kode_pos; ?>
                                </p>
                                <p>
                                    <i style="margin-right:7px" class="fa fa-phone"></i>
                                    <?php echo $telepon; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr >
                                                <th>No.</th>
                                                <th>Gambar</th>
                                                <th>Nama Barang</th>
                                                <th>Harga</th>
                                                <th>Jumlah Beli</th>
                                                <th>Jumlah Bayar</th>
                                                
                                            </tr>
                                        </thead>

                                        <tbody>
                                        <?php
                                        $no = 1;
                                        $query = mysql_query("SELECT * FROM tbl_transaksi_detail as a INNER JOIN tbl_barang as b INNER JOIN tbl_transaksi as c
                                                                        ON a.id_barang=b.id_barang AND a.id_transaksi=c.id_transaksi
                                                                        WHERE c.id_transaksi='$_GET[transaksi]'")
                                                                        or die('Ada kesalahan pada query detail transaksi: '.mysql_error());

                                        while ($data = mysql_fetch_assoc($query)) {
                                            $id_barang    = $data['id_barang'];
                                            $jumlah_beli  = $data['jumlah_beli'];
                                            $jumlah_bayar = $data['harga']*$jumlah_beli;
                                            @$totharga+=$jumlah_bayar;
											
                                        ?>
                                            <tr>
                                                <td width='40' class='center'><?php echo $no; ?></td>
                                                <td width='60'><img src="images/barang/<?php echo $data['gambar']; ?>" width="150"></td>
                                                <td width='150'><?php echo $data['nama_barang']; ?></td>
                                                <td width='120' >Rp. <?php echo format_rupiah_nol($data['harga']); ?></td>
                                                <td width='100'><?php echo $jumlah_beli; ?></td>
                                                <td width='120'>Rp. <?php echo format_rupiah_nol($jumlah_bayar); ?></td>
                                               
                                            </tr>
                                        <?php
                                            $no++;
                                        }

                                        $query1 = mysql_query("SELECT *,sum(a.harga*b.jumlah_beli) as total
                                                                        FROM tbl_barang as a INNER JOIN tbl_transaksi_detail as b
                                                                        ON a.id_barang=b.id_barang
                                                                        WHERE b.id_transaksi='$_GET[transaksi]'")
                                                                        or die('Ada kesalahan pada query total bayar: '.mysql_error());

                                        $data1 = mysql_fetch_assoc($query1);
                                        $total_bayar = $data1['total'];

                                        $query3 = mysql_query("SELECT ongkir,total_bayar FROM tbl_transaksi WHERE id_transaksi='$_GET[transaksi]'");

                                        $data3 = mysql_fetch_array($query3);
                                        $biaya_kirim = $data3['ongkir'];
                                        $total_bayar = $data3['total_bayar'];
                                        ?>
                                            <tr>
                                                <td align="right" colspan="5"><strong>Total Harga</strong></td>
                                                <td align="right"><strong>Rp. <?php echo format_rupiah_nol($totharga); ?></strong></td>
                                            </tr><tr>
                                                <td align="right" colspan="5"><strong>Biaya Kirim</strong></td>
                                                <td align="right"><strong>Rp. <?php echo format_rupiah_nol($biaya_kirim); ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="5"><strong>Total Pembayaran</strong></td>
                                                <td align="right"><strong>Rp. <?php echo format_rupiah_nol($total_bayar); ?></strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- /.panel -->

                        <div class="">
                            <a style="width:110px" href="?page=pembelian&form=view" class="btn btn-primary">Kembali</a>
                        </div>
                    </div> <!-- /.col -->
                </div> <!-- /.row -->
            </div>
        </div>
        <!-- /.row -->
    <?php
    }
}
?>
