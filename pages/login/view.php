<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">
                    <i style="margin-right:6px" class="fa fa-user"></i>
                    Login
                </h3>
            </div>
        </div>

				<?php  
                if (empty($_GET['alert'])) {
                  echo "";
                } 
                // jika alert = 1
                // tampilkan pesan Gagal "email sudah terdaftar"
                elseif ($_GET['alert'] == 1) { ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    <strong><i class="glyphicon glyphicon-alert"></i> Gagal!</strong> Email Atau Password Yang Anda Masukkan Salah, Harap Memasukkan Email Dan Password Yang Benar! , Terima Kasih.
                    </div>
                <?php
                }elseif ($_GET['alert'] == 2) { ?>
                  <div class="alert alert-danger alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  <strong><i class="glyphicon glyphicon-alert"></i> Gagal!</strong> Akun Anda belum aktif. Silahkan cek email Anda untuk melakukan aktivasi akun.
                  </div>
                <?php
                }
                ?>
				
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="pages/login/proses.php">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-5">
                                    <input type="email" class="form-control" name="email" placeholder="ponselshop@gmail.com" autocomplete="off" 		                                     required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-5">
                                    <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off" required>
                                </div>
                            </div>

                            <hr/>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <input style="width:150px" type="submit" class="btn btn-primary  btn-submit" name="login" value="Login">
									<a href="main.php?page=reset" class="btn btn-warning  btn-submit">Lupa Password</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
