<?php
  $alert_token = 1;
  // Jika token tidak ada, maka redirect kehalaman awal
  if(!isset($_GET['token'])){
    $alert_token = 2;
  }else{
    // alert_token = 1 berarti pesan konfirmasi akun berhasil diaktivasi. jika selain 1, maka munculkan pesan error
    
    $token = mysql_real_escape_string(trim($_GET['token']));
    // Cek ketersedia token didatabase
    $query_token = mysql_query("SELECT * FROM tbl_token WHERE token = ".$token);
    $row_token = mysql_num_rows($query_token);
    // Jika token tidak ditemukan, redirect ke home
    if($row_token == 0){
      $alert_token = 2;
    }else{
      $hasil_token = mysql_fetch_assoc($query_token);
      // cek apakah status token sudah 1 atau masih 0
      if($hasil_token['status'] == 1){
        // Error token sudah digunakan
        $alert_token = 2;
      }else{
        $query_konfirmasi_akun = mysql_query("UPDATE tbl_konsumen SET status = 1 WHERE email = '".$hasil_token['email']."';");
        $query_update_token = mysql_query("UPDATE tbl_token SET status = 1 WHERE token = '".$token."';");
        if(!$query_konfirmasi_akun){
          $alert_token = 0;
        }else{
          $alert_token = 1;
        }
      }
      
    }
  }
?>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">
                    <i style="margin-right:6px" class="fa fa-user"></i>
                    Konfirmasi Akun
                </h3>
            </div>
        </div>
        <div class="panel panel-default">
          <?php if($alert_token == 0): ?>
            <div class="panel-body">
              <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
              <strong><i class="glyphicon glyphicon-alert"></i> Akun gagal diaktifkan!</strong> <br/>Silahkan cek email Anda kembali dan klik link aktivasi akun yang sudah Kami kirim kembali.
              </div>
            </div>
          <?php elseif($alert_token == 1): ?>
            <div class="panel-body">
              <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
              <strong><i class="glyphicon glyphicon-alert"></i> Akun berhasil diaktifkan!</strong> <br/>Anda sudah bisa login ke sistem melalui menu <a href="?page=login">MASUK</a> dan mulai berbelanja.
              </div>
            </div>
          <?php elseif($alert_token == 2): ?>
            <div class="panel-body">
              <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
              <strong><i class="glyphicon glyphicon-alert"></i> Kesalahan!</strong> <br/> Token yang Anda gunakan tidak valid atau akun Anda sudah aktif sebelumnya.
              </div>
            </div>
          <?php endif; ?>
        </div>
    </div>
</div>
