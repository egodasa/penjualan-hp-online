
<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">
                    <i style="margin-right:6px" class="fa fa-shopping-cart"></i>
                    Cara Pembelian
                </h3>
            </div>
        </div>
                <div style="padding: 0 10px;"> 
                    
					<p style="text-align:justify">1. Daftar akun terlebih dahulu sebelum melakukan pembelian.
					</p>
					
					<p style="text-align:justify">2. Masuk menggunakan akun yang telah didaftarkan.
					</p>
					
					<p style="text-align:justify">3. Klik pada tombol beli pada barang yang ingin anda beli/pesan.
					</p>

					<p style="text-align:justify">4. Barang yang anda beli/pesan akan masuk ke dalam keranjang belanja. Anda dapat menentukan berapa jumlah yang akan dibeli, kemudian klik tombol simpan.
					</p>

					<p style="text-align:justify">5. Jika sudah selesai belanja, klik tombol selesai belanja maka akan tampil form untuk pengisian data pengiriman barang serta menentukan jenis pengiriman barang. kemudian klik tombol &#39;Proses Order&#39; maka akan tampil total pembayaran serta nomor rekening pembayaran.
					</p>
					
					<p style="text-align:justify">6. Silahkan melakukan pembayaran ke nomor rekening yang telah kami sediakan.
					</p>

					<p style="text-align:justify">7. Apabila telah melakukan pembayaran, maka barang yang dibeli/dipesan akan segera dikirimkan.
					</p>
					
					
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
