<?php
session_start();

// Panggil koneksi database.php untuk koneksi database
require_once "../../config/database.php";

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman login dan tampilkan pesan = 1
if (empty($_SESSION['user_email']) && empty($_SESSION['user_password'])){
    echo "<script type='text/javascript'>alert('Anda harus login terlebih dahulu!');</script>
		  <meta http-equiv='refresh' content='0; url=../../index.php'>";
}
// jika user sudah login, maka jalankan perintah untuk ubah password
else {
	if (isset($_POST['simpan'])) {
		// ambil data hasil submit dari form
		$id_konsumen   = mysql_real_escape_string(trim($_POST['id_konsumen']));
		$nama_konsumen = mysql_real_escape_string(trim($_POST['nama']));
		$alamat        = mysql_real_escape_string(trim($_POST['alamat']));
		$kabkota       = mysql_real_escape_string(trim($_POST['kabkota']));
		$provinsi      = mysql_real_escape_string(trim($_POST['id_prov']));
		$kode_pos      = mysql_real_escape_string(trim($_POST['kode_pos']));
		$telepon       = mysql_real_escape_string(trim($_POST['telepon']));

		// maka jalankan perintah query untuk mengubah data pada tabel konsumen
		$query = mysql_query("UPDATE tbl_konsumen SET nama_konsumen   = '$nama_konsumen',
	                                                            alamat   		= '$alamat',
	                                                            kota     		= '$kabkota',
	                                                            provinsi       	= '$provinsi',
	                                                            kode_pos        = '$kode_pos',
	                                                            telepon         = '$telepon'
	                                                   WHERE    id_konsumen     = '$id_konsumen'")
	                                    or die('Ada kesalahan pada query update : '.mysql_error());

        // cek query
        if ($query) {
            // jika berhasil tampilkan pesan berhasil update data
            header("location: ../../main.php?page=profil&alert=1");
        }
	}
}
?>
