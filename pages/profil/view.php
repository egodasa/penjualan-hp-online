<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman login dan tampilkan pesan = 1
if (empty($_SESSION['user_email']) && empty($_SESSION['user_password'])){
    echo "<script type='text/javascript'>alert('Anda harus login terlebih dahulu!');</script>
          <meta http-equiv='refresh' content='0; url=?page=home'>";
}
// jika user sudah login, maka jalankan perintah untuk ubah password
else {
	$id_k = $_SESSION['id_konsumen'];
    $query = mysql_query("SELECT * FROM tbl_konsumen WHERE id_konsumen ='$id_k'")
                                    or die('Ada kesalahan pada query tampil data konsumen: '.mysql_error());

    $data = mysql_fetch_assoc($query);

    $id_konsumen   = $data['id_konsumen'];
    $nama_konsumen = $data['nama_konsumen'];
    $alamat        = $data['alamat'];
    $id_kabkota    = $data['kota'];
    $id_provinsi   = $data['provinsi'];
    $kode_pos      = $data['kode_pos'];
    $telepon       = $data['telepon'];
	$email         = $data['email'];
?>
    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">
                        <i style="margin-right:6px" class="fa fa-user"></i>
                        Profil
                    </h3>
                   
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <?php
                    // fungsi untuk menampilkan pesan
                    // jika alert = "" (kosong)
                    // tampilkan pesan "" (kosong)
                    if (empty($_GET['alert'])) {
                      echo "";
                    }
                    // jika alert = 1
                    // tampilkan pesan Sukses "NISN sudah terdaftar"
                    elseif ($_GET['alert'] == 1) { ?>
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong><i class="glyphicon glyphicon-ok-circle"></i> Sukses!</strong> profil berhasil diubah.
                        </div>
                    <?php
                    }
                    ?>

                    <div class="panel panel-default">
                        <div class="panel-body">
                              <!-- tampilan form hubungi kami -->
                            <form class="form-horizontal" method="POST" action="pages/profil/proses.php">

                                <input type="hidden" name="id_konsumen" value="<?php echo $id_konsumen; ?>" required>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" name="nama" autocomplete="off" value="XX-30-XX" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat</label>
                                    <div class="col-sm-5">
                                        <textarea class="form-control" name="alamat" rows="3" required><?php echo $alamat; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Provinsi</label>
                                    <div class="col-sm-5">
                                        
										<?php
        											 $curl = curl_init();
        											 curl_setopt_array($curl, array(
        											 CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
        											 CURLOPT_RETURNTRANSFER => true,
        											 CURLOPT_ENCODING => "",
        											 CURLOPT_MAXREDIRS => 10,
        											 CURLOPT_TIMEOUT => 30,
        											 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        											 CURLOPT_CUSTOMREQUEST => "GET",
        											 CURLOPT_HTTPHEADER => array(
        													 "key: 9d5dfc29026612d5563df0fb3840bf96"
        											 ),
        											));
        											$response = curl_exec($curl);
        											$err = curl_error($curl);
        										?>
												<select class="form-control" name="id_prov" id="provinsi" required>
        												<option selected>XX-30-XX</option>
        													<?php
															
														$provinsi	= mysql_query("select * from tbl_konsumen where id_konsumen ='$id_konsumen'");
														$prov		= mysql_fetch_assoc($provinsi);
														
														
        															$data = json_decode($response, true);
        														
																	for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
        													?>
        															<option data-value="<?php echo $data['rajaongkir']['results'][$i]['province_id']; ?>" <?php echo $data['rajaongkir']['results'][$i]['province'] == $prov['provinsi'] ? 'selected' : null ?>> <?php echo $data['rajaongkir']['results'][$i]['province']; ?></option>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
																	<script>
																		$.ajax({
																			$('#provinsi').trigger("change");
																		});
																		
																	</script>
        													<?php 
															
															} ?>
        										</select>
                                    </div>
                                </div>

								<div class="form-group">
                                    <label class="col-sm-2 control-label">Kabupaten/Kota</label>
                                    <div class="col-sm-5">
                                        <select class="form-control" name="kabkota" id="kabupaten" required>
 										<option value='XX-30-XX' selected>XX-30-XX</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kode Pos</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" name="kode_pos" autocomplete="off" maxlength="5" onKeyPress="return goodchars(event,'0123456789',this)" value="<?php echo $kode_pos; ?>" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No. Telepon</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" name="telepon" autocomplete="off" maxlength="13" onKeyPress="return goodchars(event,'0123456789',this)" value="<?php echo $telepon; ?>" required>
                                    </div>
                                </div>
								
								  <div class="form-group">
                                    <label class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" name="email" autocomplete="off" maxlength="30" value="XX-30-XX" readonly required>
                                    </div>
                                </div>

                               
                                <hr/>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <input type="submit" class="btn btn-primary btn-submit" name="simpan" value="Simpan Perubahan">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

<?php
}
?>

<script>
$(document).ready(function(){
$('#provinsi').trigger("change");
})
</script>