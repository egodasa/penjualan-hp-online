<?php
session_start();

// Panggil koneksi database.php untuk koneksi database
require_once "../../config/database.php";

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman login dan tampilkan pesan = 1
if (empty($_SESSION['user_email']) && empty($_SESSION['user_password'])){
    echo "<script type='text/javascript'>alert('Anda harus login terlebih dahulu!');</script>
          <meta http-equiv='refresh' content='0; url=../../index.php'>";
}
// jika user sudah login, maka jalankan perintah untuk ubah password
else {
	if (isset($_GET['id_konsumen']) && isset($_GET['id_barang'])) {
		// ambil data hasil submit dari form
        $id_konsumen = mysql_real_escape_string(trim($_GET['id_konsumen']));
        $id_barang   = mysql_real_escape_string(trim($_GET['id_barang']));
        $stok        = mysql_real_escape_string(trim($_GET['stok']));
        $jumlah_beli = mysql_real_escape_string(trim($_GET['jumlah_beli']));
        $sisa_stok   = $stok + $jumlah_beli;

		// perintah query untuk menghapus data pada tabel tmp transaksi
        $query = mysql_query("DELETE FROM tbl_transaksi_tmp WHERE id_konsumen='$id_konsumen' AND id_barang='$id_barang'")
                                        or die('Ada kesalahan pada query delete : '.mysql_error());

        // cek hasil query
        if ($query) {
            // perintah query untuk mengubah data pada tabel transaksi
            $query1 = mysql_query("UPDATE tbl_barang SET stok      = '$sisa_stok'
                                                             WHERE id_barang = '$id_barang'")
                                             or die('Ada kesalahan pada query update stok : '.mysql_error());

            if ($query1) {
                // jika berhasil tampilkan pesan berhasil delete data
                header("location: ../../main.php?page=keranjang&alert=2");
            }
        }
	}
}
?>
