<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman login dan tampilkan pesan = 1
if (empty($_SESSION['user_email']) && empty($_SESSION['user_password'])){
    echo "<script type='text/javascript'>alert('Anda harus login terlebih dahulu!');</script>
          <meta http-equiv='refresh' content='0; url=?page=home'>";
}
// jika user sudah login, maka jalankan perintah untuk ubah password
else {
    $query = mysql_query("SELECT * FROM tbl_konsumen
                                    WHERE id_konsumen='$_SESSION[id_konsumen]'")
                                    or die('Ada kesalahan pada query tampil data konsumen: '.mysql_error());

    $data = mysql_fetch_assoc($query);

    $id_konsumen   = $data['id_konsumen'];
    $nama_konsumen = $data['nama_konsumen'];
    $alamat        = $data['alamat'];
    $kode_pos      = $data['kode_pos'];
    $telepon       = $data['telepon'];
    $email         = $data['email'];
?>
    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">
                        <i style="margin-right:6px" class="fa fa-shopping-cart"></i>
                        Proses Order
                    </h3>
                  
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-body">
                  <form action="pages/transaksi/proses_order.php" method="POST">
                            <h4>Alamat Tujuan</h3>
                            <div class="col-lg-10">
                              <label for="id_prov">Nama Penerima *</label>
                              <input type="text" id="nama" name="namapenerima" class="form-control" required>
                            </div>
                            <div class="col-lg-10">
                              <label for="id_prov">Provinsi *</label>
                             <?php
        											 $curl = curl_init();
        											 curl_setopt_array($curl, array(
        											 CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
        											 CURLOPT_RETURNTRANSFER => true,
        											 CURLOPT_ENCODING => "",
        											 CURLOPT_MAXREDIRS => 10,
        											 CURLOPT_TIMEOUT => 30,
        											 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        											 CURLOPT_CUSTOMREQUEST => "GET",
        											 CURLOPT_HTTPHEADER => array(
        													 "key: 9d5dfc29026612d5563df0fb3840bf96"
        											 ),
        											));
        											$response = curl_exec($curl);
        											$err = curl_error($curl);
        										?>
        										<select class="form-control" name="id_prov" id="provinsi" required>
        												<option selected>Pilih Provinsi</option>
        													<?php
        															$data = json_decode($response, true);
        															for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
        													?>
        															<option data-value="<?php echo $data['rajaongkir']['results'][$i]['province_id'] ?>"><?php echo $data['rajaongkir']['results'][$i]['province'] ?></option>
        													<?php } ?>
        										</select>
                            </div>

                            <div class="col-lg-10">
                              <label for="id_prov">Kota *</label>
                                <select class="form-control" name="id_kota" id="kabupaten" required>
                                  <option selected>Pilih Kota</option>
                                <option value='' selected>Pilih Kabupaten/Kota</option>
								</select>
                            </div>

                            <div class="col-lg-10">
                              <label for="kodepos">Kode Pos *</label>
                                 <input type="text" name="kodepos" id="kodepos" class="form-control" onkeypress="return hanyaAngka(event)" required>
                            </div>
                            <div class="col-lg-10">
                              <label for="alamat">Alamat *</label>
                                 <textarea name="alamat" id="alamat" rows="4" cols="80" class="form-control"></textarea>
                            </div>
                            <div class="col-lg-10">
                              <label for="nohp">No Hp *</label>
                                 <input type="text" name="nohp" id="nohp" class="form-control" onkeypress="return hanyaAngka(event)" required>
                            </div>
                        </div>
                    </div>
                </div>
              <div class="col-lg-6">
                <div id="formongkir">
            					<div style="padding: 10px 25px 25px 25px;">
                        <h4>Jenis Pengiriman</h4>
                        <hr>
            							<div class="form-group">
            			 					<?php
            			 						$kurir=array('jne','tiki');
            			 						foreach($kurir as $rkurir){
            					 			?>
            							 		<label class="radio-inline">
            							 			<input type="radio" name="kurir" class="kurir" value="<?php echo $rkurir; ?>"/> <?php echo strtoupper($rkurir); ?>&nbsp;&nbsp;&nbsp;&nbsp;
													
            							 		</label>
            					 			<?php
            			 					}
            			 					?>
            	 						</div>
                          <input type="hidden" id="kabupaten" value="<?php echo $id_kabkota ?>">
            							<div id="kuririnfo" style="display: none;" >
            						    <div class="form-group">
            						       <h4>Service</h4>
            						    </div>
            								<div class="form-row ml-3" id="kurirserviceinfo"></div>
            							</div>
            					</div>
            				</div>
              </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr >
                                            <th>No.</th>
                                            <th>Gambar</th>
                                            <th>Nama Barang</th>
                                            <th>Harga</th>
                                            <th>Jumlah Beli</th>
                                            <th>Jumlah Bayar</th>
                                            
                                        </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                    $no = 1;
                                    $query = mysql_query("SELECT * FROM tbl_transaksi_tmp as a INNER JOIN tbl_barang as b
                                                                    ON a.id_barang=b.id_barang
                                                                    WHERE id_konsumen='$_SESSION[id_konsumen]'")
                                                                    or die('Ada kesalahan pada query tmp transaksi: '.mysql_error());

                                    while ($data = mysql_fetch_assoc($query)) {
									$disk = ($data['diskon'] * $data['harga']) / 100;
	$hrgbaru = $data['harga'] - $disk;
                                        $id_barang    = $data['id_barang'];
                                        $jumlah_beli  = $data['jumlah_beli'];
                                        $jumlah_bayar = $data['jumlah_bayar'];
                                    ?>
                                        <tr>
                                            <td width='40' class='center'><?php echo $no; ?></td>
                                            <td width='60'><img src="images/barang/<?php echo $data['gambar']; ?>" width="150"></td>
                                            <td width='150'><?php echo $data['nama_barang']; ?></td>
                                            <td width='120' >
								 Rp. <?php echo format_rupiah_nol($hrgbaru); ?>
								</td>
                                            <td width='100'><?php echo $jumlah_beli; ?></td>
                                            <td width='120'>Rp. <?php echo format_rupiah_nol($jumlah_bayar); ?></td>
                                           
                                        </tr>
                                    <?php
                                        $no++;
                                    }

                                    $query1 = mysql_query("SELECT sum(jumlah_bayar) as total FROM tbl_transaksi_tmp
                                                                    WHERE id_konsumen='$_SESSION[id_konsumen]'")
                                                                    or die('Ada kesalahan pada query total bayar: '.mysql_error());

                                    $data1 = mysql_fetch_assoc($query1);
                                    $total_bayar = $data1['total'];


                                    $total_pembayaran = $total_bayar;

                                    $query3 = mysql_query("SELECT *, SUM(berat*jumlah_beli) AS totberat FROM tbl_barang,tbl_transaksi_tmp WHERE tbl_barang.id_barang=tbl_transaksi_tmp.id_barang AND id_konsumen='$_SESSION[id_konsumen]'");

                                    $berat = mysql_fetch_array($query3);
                                    $totb = $berat['totberat'];
									 $rand=rand(100,1000);
                                    ?>
                                        <tr>
                                            <td align="right" colspan="5"><strong>Total Harga</strong></td>
                                            <td align="right"><strong>Rp. <?php echo format_rupiah_nol($total_bayar); ?></strong></td>
                                            <input type="hidden" id="total" value="<?php echo $total_bayar; ?>">
                                            <input type="hidden" name="ongkir" id="ongkir" value="0"/>
											
                                             <input type="hidden" name="rand" id="rand" value="<?php echo $rand; ?>"/>
                                            <input type="hidden" name="totalbayar" id="totalbayar"/>
                                        </tr>
                                        <tr>
                                          <td align="right" colspan="5"><strong>Total Berat</strong></td>
                                          <td align="right"><strong><?php echo $totb." Gram"; ?></strong></td>
                                          <input type="hidden" id="berat" value="<?php echo $totb; ?>">
                                        </tr>
                                        <tr>
                                            <td align="right" colspan="5"><strong>Biaya Kirim</strong></td>
                                            <td align="right"><strong id="jmlongkir">Rp. 0</strong></td>
                                        </tr>
                                        <tr>
                                            <td align="right" colspan="5"><strong>Total Pembayaran</strong></td>
                                            <td align="right"><strong id="testotal">Rp. <?php echo format_rupiah_nol($total_pembayaran); ?></strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> <!-- /.panel -->

                    <div class="">
                        <a style="width:110px" href="?page=keranjang" class="btn btn-primary">Kembali</a>
                        &nbsp; &nbsp;
                        <button type="submit" name="order" id="oksimpan" style="display: none; float: right;" class="btn btn-primary">Proses Order</button>
                    </div>
                   </form>
                </div> <!-- /.col -->
            </div> <!-- /.row -->
        </div>
    </div>
    <!-- /.row -->
    <script>
    		function hanyaAngka(evt) {
    		  var charCode = (evt.which) ? evt.which : event.keyCode
    		   if (charCode > 31 && (charCode < 48 || charCode > 57))
    		    return false;
    		  return true;
    		}
    </script>

<?php
}
?>
