-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2018 at 11:02 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ponselshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
`id_admin` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_admin` varchar(30) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id_admin`, `username`, `password`, `nama_admin`, `level`) VALUES
(2, 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 'Super Admin', 'pemilik'),
(3, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE IF NOT EXISTS `tbl_barang` (
`id_barang` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `diskon` int(10) NOT NULL,
  `stok` int(11) NOT NULL,
  `terjual` int(11) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `gambar2` text NOT NULL,
  `gambar3` text NOT NULL,
  `gambar4` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`id_barang`, `id_kategori`, `tanggal_masuk`, `nama_barang`, `deskripsi`, `harga`, `berat`, `diskon`, `stok`, `terjual`, `gambar`, `gambar2`, `gambar3`, `gambar4`) VALUES
(1, 1, '2018-05-03', 'Samsung S9+', 'Spesifikasi Galaxy S9 Plus	Tinggi	:158,1mm Lebar	:73,8 mm Tebal	:8,5 mm Berat	:189 gram Layar	:6,2 inci Quad HD + Curved Super AMOLED, rasio 18.5:99,10, 529ppi SIM	:Dual SIM (Hybrid SIM): Nano SIM + Nano SIM or MicroSD slot Konektor	:USB Type-C, Audio jack 3,5 mm CPU	:Exynos 9810 (Indonesia) atau Snapdragon 845, 10nm, 64-bit, Octa-core processor (2,7/2,8 GHz Quad + 1,7 GHz Quad) RAM/memori	:6 GB/64 GB atau 256 GB + up to 400 GB via microSD card Sensor	:Iris sensor, Pressure sensor, Accelerometer, Barometer, Fingerprint sensor, Gyro sensor, Geomagnetic sensor, Hall sensor, HR sensor, Proximity sensor, RGB Light sensor Kamera utama	:Dual Camera with Dual OIS - Wide-angle: Super Speed Dual Pixel 12MP AF sensor (F1.5/F2.4) - Telephoto: 12MP AF sensor (F2.4) Kamera depan :Dual 16MP + 8 MP, f/1.9, 1080p Video slow-motion	:960 fps resolusi 720p/240 fps resolusi 1080p Konektivitas	:Bluetooth 5.0, NFC, Wi-Fi 802.11 a/b/g/n/ac (2.4/5GHz), VHT80 MU-MIMO, 1024QAM Baterai	:3.500 mAh Anti-debu, anti-air	:IP687 Sistem operasi	:Android 8 Oreo ', 12650000, 189, 5, 12, 6, 'samsung s9+.jpg', 'samsung s9+.jpg', 'samsung s9+.jpg', 'samsung s9+.jpg'),
(2, 2, '2018-05-02', 'Oppo F7', 'spesifikasi Oppo F7Layar	:6,23 inci, 2.280 x 1.080 piksel, aspect ratio 19:9. Dimensi fisik	:156 x 75,3 x 7,8 mm Bobot	:158 gram Prosesor	:Mediatek Helio P60 Octa core, 4 CPU ARM Cortex-A73 2 GHz + 4 CPU ARM Cortex-A53 2 GHz GPU	:Mali-G72 MP3 RAM	:4 GB/6 GB Internal storage:64 GB/128 GB, dukungan microSD hingga 256 GB Kamera depan	:25 megapiksel, lensa f/2.0 Kamera belakang	:16 megapiksel, lensa f/1.8 Video	:hingga full HD (1080p) Baterai: 3.400 mAh Konektivitas	:4G LTE (dual-SIM), Wi-Fi, Bluetooth, GPS Pengamanan	:fingerprint scanner, face recognition \r\n', 3812000, 158, 10, 6, 7, 'oppo f7.jpg', '', '', ''),
(3, 3, '2018-03-27', 'Vivo V9', 'Spesifikasi Vivo V9RAM	:4 GB ROM :64 GB Expandable Upto 256 GB Layar	:6.3 Inch Full HD+ Display Kamera Utama	:24 MP Front Kamera Belakang	:16 MP + 5 MP Dual Rear Camera Battery	:3260 mAh Li-ion Sensor	:Fingerprint Processor :Qualcomm Snapdragon 626 Sistem Operasi	:Android Oreo 8.1 ', 3329000, 150, 5, 17, 4, 'vivo v9.jpg', '', '', ''),
(4, 4, '2018-05-03', 'Blackberry Aurora', 'Spesifikasi Blackberry AuroraUkuran Layar	:5,5 inci, 1.280 x 720 (267 PPI)	Dimensi Fisik	:152 x 76,8 x 8,5 mm	Bobot	:178 gram Prosesor	:Qualcomm Snapdragon 425, CPU quad-core Cortex-A53 1,4 GHz, GPU Adreno 308	RAM	:4 GB	Media Penyimpanan 32 GB	Kamera Utama	:13 Megapiksel dengan autofocus dan LED flash	Kamera Depan	:8 Megapixel	Kapasitas Baterai	:3.000 mAh	Jaringan Seluler	:GSM/ 3G HSPA/ 4G LTE	Konektor	:Micro USB, jack audio 3,5 mm	GPS	:Ya, dengan dukungan A-GPS dan GLONASS	Kartu SIM	:Dual micro-SIM	Konektivitas	:Wi-Fi 802.11 a/b/g/n, Bluetooth 4.0	Pemindai sidik jari	:Tidak ada	Fitur lain :Accelerometer, proximity sensor, kompas, security suite DTEK, update keamanan	Sistem Operasi	:Android 7.0 Nougat ', 1925000, 178, 10, 20, 7, 'blackberry aurora.jpg', '', '', ''),
(5, 9, '2018-05-03', 'Asus Zenfone 5', 'Spesifikasi Asus Zenfone 5Display	:6.20-inchProcessor	:octa-coreFront Camera	:8-megapixelResolution	:1080x2246 pixelsRAM	:4GBOS	:Android 8.0Storage	:64GBRear Camera	:12-megapixelBattery Capacity:3300mAh \r\n', 5499000, 155, 5, 20, 1, 'asus zenfone.jpg', '', '', ''),
(6, 10, '2018-07-23', 'Iphone X', 'Spesifikasi Iphone XUkuran layar	:5,8 inci	Resolusi	:1.125 x 1.334 Prosesor	:Apple A11 Bionic RAM	:3 GB Penyimpanan	:64/256 GB microSD	:Tidak ada Kamera Belakang	:Dual 12 MegapikselKamera Depan	:7 Megapiksel NFC	:Ada Sensor fingerprint	:Tidak ada Baterai	:2.761 mAh Waterproof	:Ya OS	:iOS 11 ', 14649000, 174, 0, 20, 0, 'iphone x.jpg', '', '', ''),
(7, 6, '2018-07-23', 'Xiaomi 6x', 'Spesifikasi Xiaomi Mi 6X5.99-inch (2160?1080 pixels) Full HD+ 2.5D curved glass display with 1500:1 ContrastOcta Core Snapdragon 660 14nm Mobile Platform (Quad 2.2GHz Kryo 260 + Quad 1.8GHz Kryo 260 CPUs) with Adreno 512 GPU6GB LPDDR 4x RAM with 64GB / 128GB (eMMC 5.1) storage, 4GB LPDDR 4x RAM with 64GB storageAndroid 8.1 (Oreo) with MIUIDual SIM12MP rear camera with LED flash, f/1.75 aperture, Sony IMX486 sensor, 1.25?m pixel size, 20MP secondary camera with Sony IMX376 sensor with f/1.75 aperture, 4 in 1 ? 2.0um pixels20MP front-facing camera with Sony IMX376 sensor, 4 in 1 ? 2.0um pixels soft LED flashUSB Type-C audioFingerprint sensor, Infrared sensorDimensions: 158.7?75.4?7.3mm; Weight: 168gDual 4G VoLTE, Wi-Fi 802.11 ac (2.4GHz / 5GHz), Bluetooth 5 LE, GPS + GLONASS, USB Type-C3010mAh (typical) / 2910mAh (minimum) battery with Quick Charge 3.0 ', 4650000, 168, 0, 20, 0, 'xiaomi 6x.jpg', '', '', ''),
(8, 5, '2018-07-23', 'Nokia 6', '16MP PDAF primary camera and 8MP AF front facing camera13.97 centimeters (5.5-inch) FHD display with 1920 x 1080 pixels resolutionAndroid v7.1.1 Nougat OS with Qualcomm Snapdragon 430 octa core processor3 GB RAM, 32 GB internal memory and dual-standby (4G+4G) with Micro SD card hybrid support3000mAH lithium-ion batteryFingerprint scanner, all-metal unibody and NFC enabled, Dual Speakers with dedicated amplifier and Dolby Digital Sound ', 2450000, 145, 0, 18, 2, 'nokia 6.jpg', '', '', ''),
(9, 7, '2018-07-23', 'Aldo S11', 'Spesifikasi :\r\nSpesifikasi:\r\n5" multi touch screen\r\nAndroid 7.0 Nougat\r\nQuad core processor\r\n2GB ram + 16GB rom\r\nDual simcard, Dual standby\r\nFront camera 5mp, Rear camera 8mp\r\n4G LTE ready\r\n2500MaH battery capacity', 869000, 157, 0, 20, 0, 'aldo s11.jpg', '', '', ''),
(10, 8, '2018-07-23', 'Himax M4', 'Spesifikasi Lengkap Himax M4\r\nLayar: 5? IPS HD 1280 x 720 pixel\r\nProcessor: MediaTek MT6737 1.25GHz Quad-core\r\nGPU: Mali T720;\r\nRAM: 2GB;\r\nROM: 16GB;\r\nSlot kartu SD maksimal 128GB;\r\nKamera depan: 2 Megapixel, CMOS;\r\nKamera belakang: 8 MeSpesifikasi Lengkap Himax M4\r\nLayar: 5? IPS HD 1280 x 720 pixel\r\nProcessor: MediaTek MT6737 1.25GHz Quad-core\r\nGPU: Mali T720;\r\nRAM: 2GB;\r\nROM: 16GB;\r\nSlot kartu SD maksimal 128GB;\r\nKamera depan: 2 Megapixel, CMOS;\r\nKamera belakang: 8 Megapixel ƒ/2.2 aperture, 1080p, LED Flash;\r\nBattery: Li-Po 2000 mAh;\r\nOS Android 7.0 Nougat;\r\nKonektivitas: 4G LTE, 3G, 2G, WiFi 802.11 b/g/n, GPS, A-GPS, Bluetooth 4.0 dan Micro USB 2.0;\r\nWarna: Emas, abu-abu;\r\nDimension: 145 x 72 x 8.6 mm\r\nBerat: 146.6 gram\r\nDual SIM: Nano + Mikrogapixel ƒ/2.2 aperture, 1080p, LED Flash;\r\nBattery: Li-Po 2000 mAh;\r\nOS Android 7.0 Nougat;\r\nKonektivitas: 4G LTE, 3G, 2G, WiFi 802.11 b/g/n, GPS, A-GPS, Bluetooth 4.0 dan Micro USB 2.0;\r\nWarna: Emas, abu-abu;\r\nDimension: 145 x 72 x 8.6 mm\r\nBerat: 146.6 gram\r\nDual SIM: Nano + Mikro', 1399000, 146, 0, 17, 0, 'himax m4.jpg', '', '', ''),
(11, 11, '2018-07-24', 'Battery NOKIA BL-4C', 'Baterai Nokia tipe BL-4C ini dapat digunakan untuk beberapa handphone Nokia, antara lain:\r\n- Nokia 1202, Nokia 1661, Nokia 1662, Nokia 2220 slide\r\n- Nokia 2650, Nokia 2652, Nokia 2690, Nokia 3108, Nokia 3500 classic\r\n- Nokia 5100, Nokia 6100, Nokia 6101, Nokia 6103, Nokia 6125\r\n- Nokia 6126, Nokia 6131, Nokia 6136, Nokia 6170\r\n- Nokia 6260, Nokia 6300i, Nokia 6300, Nokia 6301\r\n- Nokia 7200, Nokia 7270, Nokia C2-05, Nokia C5-03, Nokia X2-00', 25000, 5, 0, 20, 0, 'battery nokia 4c.jpg', '', '', ''),
(12, 11, '2018-07-24', 'Battery NOKIA BL-5C', 'BL-5C dapat digunakan untuk Nokia:\r\n\r\n1100 / 1200 / 1208 / 1209 / 1280 / 1600 / 1616 / 1650 / 1680 classic / 1800 / 2300 / 2310 / 2323 classic /2330 classic / 2600 / 2610 / 2626 / 2700 classic / 2710 Navigation edition / 2730 classic / 3100 / 3109 / 3110 / 3120 / 3610 fold / 3650 / 3660 / 5030 / 5130 XpressMusic / 6030 / 6085 / 6086 / 6230 / 6230i / 6267 /6555 / 6600 / 6630 / 6670 / 6680 / 6681 / 7610 / c1-01 / c1-02 / E50 / E60 / N70 / N71 / N72 / N91 dan untuk type nokia lain nya', 25000, 5, 0, 26, 0, 'battery nokia 5c.jpg', '', '', ''),
(13, 12, '2018-07-24', 'Headset Samsung HS330', 'Suara Jernih\r\nOriginal\r\nFast Respon\r\nDisertai Mic\r\nMengkombinasi Integrated Headset Technology Dengan Stereo Sound Isolating Design Untuk Memblokir Suara Luar Sementara Menghasilkan Suara Jernih Untuk Telinga Anda.', 25000, 5, 0, 30, 0, 'Headset Samsung HS330.jpg', '', '', ''),
(14, 12, '2018-08-10', 'Headset Bluetooth SBH-503', 'Headset Bluetooth SAMSUNG SBH-503 untuk Musik-an dan Telepon-an\r\n\r\nSupport untuk semua type HP yang support Bluetooth\r\nSuara Jernih Mantapp dan Mega Bass\r\n- Model: SBH-503\r\n- Color: - Black + Red\r\n- Enjoy high-quality stereo sound streamed from your compatible phone\r\n- Stay comfortably in style with over the ear design and an easygoing neckband\r\n- Control the music player and manage your calls with the media keys\r\n- Bluetooth 2.0 + EDR\r\n- Operating Range: up to 10m\r\n- Talk time: 6 hours (manufacturer rated)60\r\n- Stand by time: 80 hours (manufacturer rated)', 160000, 60, 5, 10, 0, 'Headset Bluetooth SBH-503.jpg', '', '', ''),
(15, 13, '2018-08-10', 'Speaker Bluetooth Mini', 'Compatible Rechargeable Bluetooth Speaker WITH LED Wireless Audio Receiver Outdoor, Home Theatre Portable USB MP3 Player Stereo Surround Loud Mini Radio Bluetooth Speaker Speakers with Light Support TF Card and Aux with MIC and Phone Call Receiving Feature Support 3.5 MM audio jack + Charging Cable Assorted Color', 65000, 55, 10, 10, 0, 'Speaker Bluetooth Mini.png', '', '', ''),
(16, 4, '2018-10-30', 'jojo', 'jkjk', 900000, 6, 0, 10, 0, 'habibi.jpg', 'tes.PNG', 'user.PNG', 'utama.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE IF NOT EXISTS `tbl_kategori` (
`id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Samsung'),
(2, 'Oppo'),
(3, 'Vivo'),
(4, 'Blackberry'),
(5, 'Nokia'),
(6, 'Xiaomi'),
(7, 'Aldo'),
(8, 'Himax'),
(9, 'Asus'),
(10, 'Iphone'),
(11, 'Battery'),
(12, 'Headset'),
(13, 'Speaker'),
(14, 'Charger'),
(15, 'Tempered Glass'),
(16, 'Powerbank'),
(17, 'Kabel Data USB'),
(18, 'Flashdisk');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_komentar`
--

CREATE TABLE IF NOT EXISTS `tbl_komentar` (
`id_komentar` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_konsumen` int(11) NOT NULL,
  `komentar` text NOT NULL,
  `status` enum('y','n') NOT NULL DEFAULT 'n',
  `balas` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_komentar`
--

INSERT INTO `tbl_komentar` (`id_komentar`, `id_barang`, `tanggal`, `id_konsumen`, `komentar`, `status`, `balas`) VALUES
(2, 4, '2018-08-03 14:07:04', 0, 'fdsaf', 'y', 1),
(3, 7, '2018-08-06 10:51:39', 66, 'mantap', 'n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_konsumen`
--

CREATE TABLE IF NOT EXISTS `tbl_konsumen` (
`id_konsumen` int(11) NOT NULL,
  `tanggal_daftar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nama_konsumen` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kota` varchar(30) NOT NULL,
  `provinsi` varchar(30) NOT NULL,
  `kode_pos` varchar(5) NOT NULL,
  `telepon` varchar(12) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_konsumen`
--

INSERT INTO `tbl_konsumen` (`id_konsumen`, `tanggal_daftar`, `nama_konsumen`, `alamat`, `kota`, `provinsi`, `kode_pos`, `telepon`, `email`, `password`) VALUES
(66, '2018-07-23 08:35:24', 'rudi', 'tanjung pura', 'Pontianak', 'Kalimantan Barat', '78113', '081528687171', 'rudi@gmail.com', '1755e8df56655122206c7c1d16b1c7e3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi`
--

CREATE TABLE IF NOT EXISTS `tbl_transaksi` (
`id_transaksi` int(11) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `id_konsumen` int(11) NOT NULL,
  `kurir` varchar(50) NOT NULL,
  `ongkir` int(11) NOT NULL,
  `total_bayar` varchar(15) NOT NULL,
  `nama_penerima` varchar(100) NOT NULL,
  `id_prov` varchar(30) NOT NULL,
  `id_kota` varchar(30) NOT NULL,
  `kodepos` varchar(10) NOT NULL,
  `alamat` text NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `statuspengiriman` varchar(50) NOT NULL,
  `tanggal_bayar` date NOT NULL,
  `rekening_asal` varchar(10) NOT NULL,
  `no_rekening_asal` varchar(20) NOT NULL,
  `pemilik_rekening` varchar(40) NOT NULL,
  `rekening_tujuan` varchar(10) NOT NULL,
  `jumlah_bayar` varchar(15) NOT NULL,
  `bukti_bayar` varchar(150) NOT NULL,
  `status_bayar` varchar(50) NOT NULL,
  `no_resi` varchar(20) NOT NULL,
  `tgl_kirim` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`id_transaksi`, `tanggal_transaksi`, `id_konsumen`, `kurir`, `ongkir`, `total_bayar`, `nama_penerima`, `id_prov`, `id_kota`, `kodepos`, `alamat`, `nohp`, `statuspengiriman`, `tanggal_bayar`, `rekening_asal`, `no_rekening_asal`, `pemilik_rekening`, `rekening_tujuan`, `jumlah_bayar`, `bukti_bayar`, `status_bayar`, `no_resi`, `tgl_kirim`) VALUES
(24, '2018-07-23', 66, 'jne', 6000, '9103000', 'budi', 'Kalimantan Barat', 'Pontianak', '78113', 'sei raya dalam', '343412235124324', 'Transaksi Selesai', '2018-07-23', 'BI', '324', 'fsfdsfa', 'BNI', '9103000', 'Koala.jpg', 'Pembayaran Diterima', '1', '0000-00-00'),
(25, '2018-07-28', 66, 'jne', 41000, '3370000', 'dfsaf', 'Bangka Belitung', 'Bangka Selatan', '1414', 'fdsaf', '324', 'Transaksi Selesai', '2018-07-28', 'BNI', '32432', 'fdsaf', 'BNI', '3370000', 'Chrysanthemum.jpg', 'Pembayaran Diterima', '123', '0000-00-00'),
(26, '2018-07-31', 66, 'jne', 6000, '11492000', 'rudi', 'Kalimantan Barat', 'Pontianak', '78113', 'jl.tanjung pura no.11', '081528687171', 'Pesanan Dibatalkan', '0000-00-00', '', '', '', '', '', '', 'Menunggu Pembayaran', '', '0000-00-00'),
(27, '2018-08-01', 66, 'tiki', 34000, '45778000', 'fddd', 'Kalimantan Barat', 'Melawi', '24', 'fdfdff', '3235352', 'Pesanan Dibatalkan', '0000-00-00', '', '', '', '', '', '', 'Menunggu Pembayaran', '', '0000-00-00'),
(28, '2018-08-03', 66, 'tiki', 25000, '25325000', 'gfdsgd', 'Kalimantan Barat', 'Ketapang', '253423', 'fdsfadsf', '5253325', 'Transaksi Selesai', '2018-08-03', 'BTN', '457678', 'fghghh', 'BNI', '25325000', 'Penguins.jpg', 'Pembayaran Diterima', '12', '0000-00-00'),
(29, '2018-08-04', 66, 'jne', 6000, '11492000', 'rudi', 'Kalimantan Barat', 'Pontianak', '78113', 'jl.tanjung pura no.11', '081528687171', 'Transaksi Selesai', '2018-08-06', 'Mandiri', '3242', 'ffsdfsdaf', 'BNI', '11492000', 'Penguins.jpg', 'Pembayaran Diterima', '1234', '0000-00-00'),
(30, '2018-08-06', 53, 'jne', 26000, '3355000', 'fddd', 'Banten', 'Pandeglang', '3254', 'fdsfsdaf', '5253325', 'Menunggu Verifikasi Pembayaran', '2018-08-06', 'BI', '325', 'efsdf', 'BNI', '3355000', 'Penguins.jpg', 'Menunggu Verifikasi Pembayaran', '', '0000-00-00'),
(31, '2018-08-06', 66, 'jne', 12000, '3341000', 'fddd', 'Kalimantan Barat', 'Ketapang', '253423', 'dffew', '3254', 'Transaksi Selesai', '2018-08-06', 'Mandiri', '5325', 'ffa', 'BNI', '3341000', 'Penguins.jpg', 'Pembayaran Diterima', '12345', '0000-00-00'),
(32, '2018-08-06', 66, 'jne', 60000, '3389000', 'dfsaf', 'Kalimantan Timur', 'Kutai Barat', '324', 'fsfa', '324', 'Transaksi Selesai', '2018-08-06', 'BI', '32', 'fsfd', 'BNI', '3389000', 'Hydrangeas.jpg', 'Pembayaran Diterima', '123456', '0000-00-00'),
(33, '2018-08-06', 66, 'jne', 60000, '25360356', 'Rudi', 'Jambi', 'Bungo', '25146', 'Jl. Muara Takus No 21', '082170214455', 'Transaksi Selesai', '2018-08-06', 'Mandiri', '67676767', 'Rudi', 'BCA', '25360356', 'nofoto.jpg', 'Pembayaran Diterima', '565656556', '2018-08-06'),
(34, '2018-09-04', 66, 'jne', 17000, '3448586', 'Rudi', 'DKI Jakarta', 'Jakarta Barat', '25146', 'Padang', '082170214455', 'Sedang Dikemas', '2018-09-04', 'BNI', '6777676', 'Rudi', 'BNI', '3448586', 'nofoto.jpg', 'Pembayaran Diterima', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi_detail`
--

CREATE TABLE IF NOT EXISTS `tbl_transaksi_detail` (
`id_detail` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah_beli` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaksi_detail`
--

INSERT INTO `tbl_transaksi_detail` (`id_detail`, `id_transaksi`, `id_barang`, `jumlah_beli`) VALUES
(42, 24, 10, 3),
(43, 24, 8, 2),
(44, 25, 3, 1),
(45, 26, 2, 3),
(46, 26, 12, 2),
(47, 27, 2, 12),
(48, 28, 1, 2),
(49, 29, 2, 3),
(50, 29, 12, 2),
(51, 30, 3, 1),
(52, 31, 3, 1),
(53, 32, 3, 1),
(54, 33, 1, 2),
(55, 34, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi_tmp`
--

CREATE TABLE IF NOT EXISTS `tbl_transaksi_tmp` (
  `id_konsumen` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah_beli` int(11) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaksi_tmp`
--

INSERT INTO `tbl_transaksi_tmp` (`id_konsumen`, `id_barang`, `jumlah_beli`, `jumlah_bayar`) VALUES
(51, 3, 1, 6960000),
(51, 3, 2, 13920000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
 ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
 ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
 ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tbl_komentar`
--
ALTER TABLE `tbl_komentar`
 ADD PRIMARY KEY (`id_komentar`), ADD KEY `id_warta` (`id_barang`), ADD KEY `id_barang` (`id_barang`), ADD KEY `id_konsumen` (`id_konsumen`), ADD KEY `id_konsumen_2` (`id_konsumen`);

--
-- Indexes for table `tbl_konsumen`
--
ALTER TABLE `tbl_konsumen`
 ADD PRIMARY KEY (`id_konsumen`), ADD KEY `kota` (`kota`), ADD KEY `provinsi` (`provinsi`);

--
-- Indexes for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
 ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `tbl_transaksi_detail`
--
ALTER TABLE `tbl_transaksi_detail`
 ADD PRIMARY KEY (`id_detail`), ADD KEY `id_transaksi` (`id_transaksi`,`id_barang`), ADD KEY `id_barang` (`id_barang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_komentar`
--
ALTER TABLE `tbl_komentar`
MODIFY `id_komentar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_konsumen`
--
ALTER TABLE `tbl_konsumen`
MODIFY `id_konsumen` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tbl_transaksi_detail`
--
ALTER TABLE `tbl_transaksi_detail`
MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
