<?php
session_start();

// Panggil koneksi database.php untuk koneksi database
require_once "../../../config/database.php";

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman login dan tampilkan pesan = 1
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=index.php?alert=1'>";
}
// jika user sudah login, maka jalankan perintah untuk update
else {
    if ($_GET['act']=='terima') {
        if (isset($_GET['bayar'])) {
            // ambil data hasil submit dari form
            $id_transaksi = mysql_real_escape_string(trim($_GET['transaksi']));
            $id_barang    = mysql_real_escape_string(trim($_GET['barang']));
			 $jumlah    = mysql_real_escape_string(trim($_GET['jumlah']));
            $terjual      = mysql_real_escape_string(trim($_GET['terjual'])) + mysql_real_escape_string(trim($_GET['jumlah']));

            $status_bayar = 'Pembayaran Diterima';
            $status       = 'Sedang Dikemas';

            // perintah query untuk mengubah data pada tabel pembayaran
            $query = mysql_query("UPDATE tbl_transaksi SET status_bayar  = '$status_bayar'
                                                                WHERE id_transaksi      = '$id_transaksi'")
                                                        or die('Ada kesalahan pada query update : '.mysql_error());

            // cek query
            if ($query) {
                // perintah query untuk mengubah data pada tabel transaksi
                $query1 = mysql_query("UPDATE tbl_transaksi SET statuspengiriman        = '$status'
                                                                    WHERE id_transaksi  = '$id_transaksi'")
                                                            or die('Ada kesalahan pada query update : '.mysql_error());

                if ($query1) {
                    // perintah query untuk mengubah data pada tabel transaksi
                    $query2 = mysql_query("UPDATE tbl_barang SET stok   = stok - '$jumlah',terjual   = '$terjual'
                                                                     WHERE id_barang = '$id_barang'")
                                                                or die('Ada kesalahan pada query update : '.mysql_error());

                    if ($query2) {
                        // jika berhasil tampilkan pesan berhasil update data
                        header("location: ../../main.php?module=konfirmasi&alert=1");
                    }
                }
            }
        }
    }

    elseif ($_GET['act']=='tolak') {
        if (isset($_GET['bayar'])) {
            // ambil data hasil submit dari form
            $id_transaksi = mysql_real_escape_string(trim($_GET['transaksi']));

            $status       = 'Pembayaran Ditolak';

            // perintah query untuk mengubah data pada tabel pembayaran
            $query = mysql_query("UPDATE tbl_transaksi SET status_bayar  = '$status'
                                                                WHERE id_transaksi      = '$id_transaksi'")
                                                        or die('Ada kesalahan pada query update : '.mysql_error());

            // cek query
            if ($query) {
                // perintah query untuk mengubah data pada tabel transaksi
                $query1 = mysql_query("UPDATE tbl_transaksi SET status_bayar        = '$status'
                                                                    WHERE id_transaksi  = '$id_transaksi'")
                                                            or die('Ada kesalahan pada query update : '.mysql_error());

                if ($query1) {
                    // jika berhasil tampilkan pesan berhasil update data
                    header("location: ../../main.php?module=konfirmasi&alert=2");
                }
            }
        }
    }
}
?>
