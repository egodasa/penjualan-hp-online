<?php
session_start();

// Panggil koneksi database.php untuk koneksi database
require_once "../../../config/database.php";

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman login dan tampilkan pesan = 1
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=index.php?alert=1'>";
}
// jika user sudah login, maka jalankan perintah untuk update
else {
    if (isset($_POST['simpan'])) {
        if (isset($_POST['id_informasi'])) {
            // ambil data hasil submit dari form
            $id         = mysql_real_escape_string(trim($_POST['id_informasi']));
            $judul      = mysql_real_escape_string(trim($_POST['judul']));
            $keterangan = mysql_real_escape_string(trim($_POST['keterangan']));

            // perintah query untuk mengubah data pada tabel infomarsi
            $query = mysql_query("UPDATE tbl_informasi SET judul          = '$judul',
                                                                     keterangan     = '$keterangan'
                                                               WHERE id_informasi   = '$id'")
                                            or die('Ada kesalahan pada query update : '.mysql_error());

            // cek query
            if ($query) {
                // jika berhasil alihkan ke halaman cara beli
                header("location: ../../main.php?module=cara_beli");
            }
        }
    }
}
?>
