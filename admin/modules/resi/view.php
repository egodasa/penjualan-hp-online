<div class="page-content">
	<div class="page-header">
		<h1 style="color:#585858">
			<i style="margin-right:7px" class="ace-icon fa fa-shopping-cart"></i> Data Pengiriman
		</h1>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="row">
				<div class="col-xs-12">
					<div class="table-header">
						Data Pengiriman Barang
					</div>
					<!-- div.table-responsive -->

					<!-- div.dataTables_borderWrap -->
					<div>
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>No.</th>
									<th>Kode Pesanan</th>
									<th>Provinsi</th>
									<th>Kota</th>
                  <th>Layanan</th>
                  <th>Status</th>
									<th>No Resi</th>
									<th>Tanggal Kirim</th>
									<th></th>
								</tr>
							</thead>

							<tbody>
							<?php
                $no = 1;
                $query = mysql_query("SELECT * FROM tbl_transaksi WHERE statuspengiriman='Proses Pengiriman' OR statuspengiriman='Transaksi Selesai' OR statuspengiriman='Sedang Dikemas' ORDER BY id_transaksi DESC")
                              or die('Ada kesalahan pada query transaksi: '.mysql_error());

                while ($data = mysql_fetch_assoc($query)) {
                  $tgl               = substr($data['tgl_kirim'],0,10);
                  $exp               = explode('-',$tgl);
                  $tanggal_transaksi = tgl_eng_to_ind($exp[2]."-".$exp[1]."-".$exp[0]);

                  
                ?>
                <tr>
									<td width="40" class="center"><?php echo $no; ?></td>
									
									<td width="150">TB000<?php echo $data['id_transaksi']; ?></td>
									<td width="70"><?php echo $data['id_prov']; ?></td>
									<td width="100"><?php echo $data['id_kota']; ?></td>
									<td width="100"><?php echo $data['kurir']; ?></td>
									<td width="150"><?php echo $data['statuspengiriman']; ?></td>
									<td width="120"><?php echo $data['no_resi']; ?></td>
									<td width="100" class="center"><?php echo $tanggal_transaksi; ?></td>

									<td width="60" class="center">
										<div class="action-buttons">
											<?php if($data['no_resi']=="")  { ?>
											<a data-rel="tooltip" data-placement="top" title="Input Resi" class="blue tooltip-info" href="?module=form_resi&form=detail&id=<?php echo $data['id_transaksi']; ?>&id_konsumen=<?php echo $data['id_konsumen']; ?>">
												<i class="ace-icon fa fa-plus"></i>
											</a>
											<?php } else if ($data['statuspengiriman']=="Transaksi Selesai") {?>
												<a data-rel="tooltip" data-placement="top" title="Selesai" class="blue tooltip-info">
												<i class="ace-icon fa fa-check"></i>
											</a>
											<?php } else { ?>
											<a data-rel="tooltip" data-placement="top" title="Ubah Status" class="blue tooltip-info" href="modules/ubah_status/proses.php?id=<?php echo $data['id_transaksi']; ?>&id_konsumen=<?php echo $data['id_konsumen']; ?>" target="_blank">
												<i class="ace-icon fa fa-refresh"></i>
											</a>
										
											<?php } ?>

											<a data-rel="tooltip" data-placement="top" title="Cetak" class="blue tooltip-info" href="modules/resi/cetakpengiriman.php?id=<?php echo $data['id_transaksi']; ?>&id_konsumen=<?php echo $data['id_konsumen']; ?>" target="_blank">
												<i class="ace-icon fa fa-search-plus bigger-130"></i>
											</a>
											
											</div>
									</td>
								</tr>
							<?php
                $no++;
                }
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div><!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->
