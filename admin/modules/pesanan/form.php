<?php
// fungsi untuk pengecekan tampilan form
// jika form detail data yang dipilih
if ($_GET['form']=='detail') {
	 $query = mysql_query("SELECT * FROM tbl_transaksi WHERE id_transaksi='$_GET[id]'")
                                    or die('Ada kesalahan pada query tampil data konsumen: '.mysql_error());

    $data = mysql_fetch_assoc($query);

		$nmpenerima   = $data['nama_penerima'];
		$alamat = $data['alamat'];
		$nama_kabkota = $data['id_kota'];
		$nama_provinsi = $data['id_prov'];
		$kode_pos = $data['kodepos'];
		$telepon = $data['nohp'];
?>
 	<!-- tampilkan form detail data -->
	<div class="page-content">
		<div class="page-header">
			<h1 style="color:#585858">
				<i class="ace-icon fa fa-edit"></i>
				Detail Pesanan
			</h1>
		</div><!-- /.page-header -->

		<div class="row">
			<div class="col-xs-12">
				<!--PAGE CONTENT BEGINS-->
				<div class="timeline-item clearfix">
					<div style="margin-left:0" class="widget-box transparent">
						<div class="widget-header widget-header-small">
							<h5 class="widget-title smaller">
								<span class="blue">Alamat Tujuan</span>
							</h5>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<p>
                                    <i style="margin-right:7px" class="fa fa-user"></i>
                                    <?php echo $nmpenerima; ?>
                                </p>
                                <p>
                                    <i style="margin-right:7px" class="fa fa-map-marker"></i>
                                    <?php echo $alamat; ?>, <?php echo $nama_kabkota; ?>, <?php echo $nama_provinsi; ?>, <?php echo $kode_pos; ?>
                                </p>
                                <p>
                                    <i style="margin-right:7px" class="fa fa-phone"></i>
                                    <?php echo $telepon; ?>
                                </p>
							</div>
						</div>
					</div>
				</div>

				<br>

				<div class="row">
					<div class="col-xs-12">
						<div>
							<table id="simple-table" class="table table-striped table-bordered table-hover">
								<thead>
	                                <tr >
	                                    <th>No.</th>
	                                    <th>Gambar</th>
	                                    <th>Nama Produk</th>
	                                    <th>Harga</th>
	                                    <th>Jumlah Beli</th>
	                                    <th>Jumlah Bayar</th>
	                                </tr>
	                            </thead>

	                            <tbody>
	                            <?php
	                            $no = 1;
	                            $query = mysql_query("SELECT * FROM tbl_transaksi_detail as a INNER JOIN tbl_barang as b INNER JOIN tbl_transaksi as c
	                                                            ON a.id_barang=b.id_barang AND a.id_transaksi=c.id_transaksi
	                                                            WHERE c.id_transaksi='$_GET[id]'")
	                                                            or die('Ada kesalahan pada query detail transaksi: '.mysql_error());

	                            while ($data = mysql_fetch_assoc($query)) {
	                                $id_barang    = $data['id_barang'];
	                                $jumlah_beli  = $data['jumlah_beli'];
	                                $jumlah_bayar = $data['harga']*$jumlah_beli;
																	@$totbayar += $jumlah_bayar;
																	$ongkir = $data['ongkir'];
																	$alltotal = $totbayar+$ongkir;
	                            ?>
	                                <tr>
	                                    <td width='40' class='center'><?php echo $no; ?></td>
	                                    <td width='60' class="center"><img src="../images/barang/<?php echo $data['gambar']; ?>" width="100"></td>
	                                    <td width='180'><?php echo $data['nama_barang']; ?></td>
	                                    <td width='120' align="right">Rp. <?php echo format_rupiah_nol($data['harga']); ?></td>
	                                    <td width='100' class="center"><?php echo $jumlah_beli; ?></td>
	                                    <td width='120' align="right">Rp. <?php echo format_rupiah_nol($jumlah_bayar); ?></td>
	                                </tr>
	                            <?php
	                                $no++;
	                            }
	                            ?>
	                                <tr>
	                                    <td align="right" colspan="5"><strong>Total Harga</strong></td>
	                                    <td align="right"><strong>Rp. <?php echo format_rupiah_nol($totbayar); ?></strong></td>
	                                </tr><tr>
	                                    <td align="right" colspan="5"><strong>Biaya Kirim</strong></td>
	                                    <td align="right"><strong>Rp. <?php echo format_rupiah_nol($ongkir); ?></strong></td>
	                                </tr>
	                                <tr>
	                                    <td align="right" colspan="5"><strong>Total Pembayaran</strong></td>
	                                    <td align="right"><strong>Rp. <?php echo format_rupiah_nol($alltotal); ?></strong></td>
	                                </tr>
	                            </tbody>
							</table>
						</div>
					</div>
				</div><!-- PAGE CONTENT ENDS -->

				<div class="clearfix form-actions">
					<div class="col-md-offset-0 col-md-12">
						<a style="width:100px" href="?module=pesanan" class="btn">Kembali</a>
					</div>
				</div>
				<!--PAGE CONTENT ENDS-->
			</div><!--/.span-->
		</div><!--/.row-fluid-->
	</div><!--/.page-content-->
<?php
}
?>
