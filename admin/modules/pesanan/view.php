<div class="page-content">
	<div class="page-header">
		<h1 style="color:#585858">
			<i style="margin-right:7px" class="ace-icon fa fa-shopping-cart"></i> Data Pesanan
		</h1>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="row">
				<div class="col-xs-12">
					<div class="table-header">
						Data Pesanan Produk
					</div>
					<!-- div.table-responsive -->

					<!-- div.dataTables_borderWrap -->
					<div>
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>No.</th>
									<th>Tanggal Transaksi</th>
									<th>Konsumen</th>
									<th>Jumlah</th>
                  <th>Total Pembayaran</th>
                  <th>Status</th>
									<th>No Resi</th>
									<th></th>
								</tr>
							</thead>

							<tbody>
							<?php
                            $no = 1;
                            $query = mysql_query("SELECT * FROM tbl_transaksi as a INNER JOIN tbl_konsumen as b
                            								ON a.id_konsumen=b.id_konsumen
                            								ORDER BY a.id_transaksi DESC")
                                                            or die('Ada kesalahan pada query transaksi: '.mysql_error());

                            while ($data = mysql_fetch_assoc($query)) {
                                $tgl               = substr($data['tanggal_transaksi'],0,10);
                                $exp               = explode('-',$tgl);
                                $tanggal_transaksi = tgl_eng_to_ind($exp[2]."-".$exp[1]."-".$exp[0]);

                                $query1 = mysql_query("SELECT COUNT(id_detail) as jumlah FROM tbl_transaksi_detail
                                                            WHERE id_transaksi='$data[id_transaksi]'")
                                                            or die('Ada kesalahan pada query detail: '.mysql_error());

                                $data1 = mysql_fetch_assoc($query1);
								
								$tgljthtmp		= date("d-m-Y",strtotime("+3 days", strtotime($data["tanggal_transaksi"])));
											$tglskrng		= date("d-m-Y");
											$idt	= $data["id_transaksi"];
											
											if (($data['statuspengiriman']=="Menunggu Pembayaran") && ($tgljthtmp < $tglskrng)) {
												$batalpesanan	= mysql_query("update tbl_transaksi set statuspengiriman = 'Pesanan Dibatalkan'
													where id_transaksi = '$idt'") or die (mysql_error());
													}
											
                            ?>
                            	<tr>
									<td width="40" class="center"><?php echo $no; ?></td>
									<td width="100" class="center">YYYY-MM-DD</td>
									<td width="150">XX-30-XX</td>
									<td width="70" class="center">999 barang</td>
									<td width="100" align="right">Rp. 999999999</td>
									<td width="150">XX-50-XX</td>
									<td width="120">XX-25-XX</td>

									<td width="60" class="center">
										<div class="action-buttons">
											<a data-rel="tooltip" data-placement="top" title="Detail" class="blue tooltip-info" href="?module=form_pesanan&form=detail&id=<?php echo $data['id_transaksi']; ?>&id_konsumen=<?php echo $data['id_konsumen']; ?>">
												<i class="ace-icon fa fa-search-plus bigger-130"></i>
											</a>
										</div>
									</td>
								</tr>
							<?php
                            	$no++;
                            } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div><!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->
