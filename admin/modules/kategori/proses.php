<?php
session_start();

// Panggil koneksi database.php untuk koneksi database
require_once "../../../config/database.php";

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman login dan tampilkan pesan = 1
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=index.php?alert=1'>";
}
// jika user sudah login, maka jalankan perintah untuk insert, update, dan delete
else {
    if ($_GET['act']=='insert') {
        if (isset($_POST['simpan'])) {
            // ambil data hasil submit dari form
             $sql=mysql_query("SELECT nama_kategori FROM tbl_kategori WHERE nama_kategori='$_POST[nama_kategori]'");
            
            $nama_kategori = mysql_real_escape_string(trim($_POST['nama_kategori']));

            // perintah query untuk menyimpan data ke tabel kategori
            if($cek=mysql_num_rows($sql)>0){
               echo "<script>alert('Nama Kategori Sudah Ada');
                    window.location='../../main.php?module=form_kategori&form=add';
                </script>";
            }else{
            $query = mysql_query("INSERT INTO tbl_kategori(nama_kategori)
                                            VALUES('$nama_kategori')")
                                            or die('Ada kesalahan pada query insert : '.mysql_error());

            // cek query
            if ($query) {
                // jika berhasil tampilkan pesan berhasil simpan data
                header("location: ../../main.php?module=kategori&alert=1");
            }
        }
        }
    }

    elseif ($_GET['act']=='update') {
        if (isset($_POST['simpan'])) {
            if (isset($_POST['id_kategori'])) {
                // ambil data hasil submit dari form
                $id_kategori   = mysql_real_escape_string(trim($_POST['id_kategori']));
                $nama_kategori = mysql_real_escape_string(trim($_POST['nama_kategori']));

                // perintah query untuk mengubah data pada tabel kategori
                $query = mysql_query("UPDATE tbl_kategori SET nama_kategori = '$nama_kategori'
                                                                  WHERE id_kategori   = '$id_kategori'")
                                                or die('Ada kesalahan pada query update : '.mysql_error());

                // cek query
                if ($query) {
                    // jika berhasil tampilkan pesan berhasil update data
                    header("location: ../../main.php?module=kategori&alert=2");
                }
            }
        }
    }

    elseif ($_GET['act']=='delete') {
        if (isset($_GET['id'])) {
            $id_kategori = $_GET['id'];

            // perintah query untuk menghapus data pada tabel kategori
            $query = mysql_query("DELETE FROM tbl_kategori WHERE id_kategori='$id_kategori'")
                                            or die('Ada kesalahan pada query delete : '.mysql_error());

            // cek hasil query
            if ($query) {
                // jika berhasil tampilkan pesan berhasil delete data
                header("location: ../../main.php?module=kategori&alert=3");
            }
        }
    }
}
?>
